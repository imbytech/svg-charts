import yargs from 'yargs/yargs';
import { hideBin } from 'yargs/helpers';

const argv = yargs(hideBin(process.argv)).parseSync();
const [ src, dest, tmpl ] = argv['_'];

console.log(src, dest, tmpl);
